In order to activate the packages found here, just do:
```
opam repository add --kind=git camlcity https://gitlab.camlcity.org/gerd/opam.git
```
followed by
```
opam update
```

